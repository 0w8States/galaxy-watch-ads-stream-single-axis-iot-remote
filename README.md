# Galaxy Watch - IoT Single Axis Controller

This project is for an MQTTnet implementation on a Samsung Galaxy Watch with Tizen 4 OS. The project is built as a Xamarin Wearable and allows the user to control an industrial servo through MQTT connection to Beckhoff TwinCAT 3.1. 

There will be future repositories for 6-Axis robotic control and standard MQTT status notifications. For now, this one is 90% complete and only really lacks the OnResume and OnSleep functionality. If the watch goes to sleep at the current state, it most likely will need to be forced closed and re-opened. This will be taken care of over the upcoming weeks.


- Keep in mind that you will need to register a project certificate to download the app to the watch. Inside VS you will need to also activate the certificate by going to Tools -> Options -> Tizen.


![Single Axis App](TizenGalaxyWatch_IoTSingleAxisController/Image/watch.png)

